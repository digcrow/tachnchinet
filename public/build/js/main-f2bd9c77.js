var facebookUtils = (function($) {
  var token, loginStatus, loginCallback, oldValue;

  function handleRedirectLogin() {
    var queryString = window.location.hash.substring(1);
    window.location.hash = '';
    var params = queryString.split('&');
    var accessToken;

    for (var i in params) {
      var pair = params[i].split("=");
      if (pair[0] == 'access_token')
        accessToken = pair[1];
    }

    if ( accessToken ) {
      config.targetButton.click();
    }
  }

  var config = {
    appId: '',
    scopes: [],
    requiredScopes : [],
    targetButton: null,
    onComplete: function() {
    }
  }

  function init(options) {
    $.extend(config, config, options);
    disableTargetButton();

    $.ajax({
      url: '//connect.facebook.net/fr_FR/sdk.js',
      dataType: "script",
      cache: true
    }).done(function() {
      FB.init({
        appId: config.appId,
        status: true,
        xfbml: true,
        version: 'v2.3'
      });

      setLoginStatus(function(){
        enableTargetButton();
        handleRedirectLogin();
        config.onComplete();
      });
    });
  }

  function setLoginStatus(callback) {
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        token = response.authResponse.accessToken;

        checkPermissions(function(granted) {
          loginStatus = (granted) ? 'connected' : 'missing_permissions' ;
          callback();
        });
      } else {
        loginStatus = response.status;
        callback();
      }
    });
  }

  function checkPermissions(callback) {
    FB.api('/me/permissions', function(response) {
      var currentPermissions = $.map(response.data, function(item) {
        return item.permission;
      });

      for (var i in config.requiredScopes) {
        var item = currentPermissions.indexOf(config.requiredScopes[i]);
        if ( item == -1 || response.data[item].status === 'declined' ) {
          return callback(false);
        }
      }
      callback(true);
    });
  }

  function handleMissingPermissions(grantedScopes) {
    for (var i in config.requiredScopes) {
      if ( grantedScopes.indexOf(config.requiredScopes[i]) == -1 ) {
        sweetAlert('Permissions requises', 'Pour vous offrir une expérience personnalisée, vous devez autoriser l\'application à accéder à ces informations: ', 'info');
        return false;
      }
    }
    return true;
  }

  function sendLoginRequest() {
    disableTargetButton();
    $.ajax({
      url: BASE_URL + "/auth/facebookLogin",
      data: {token: token}
    }).done(function(response) {
      enableTargetButton();
      loginCallback(response);
    });
  }

  function promptLogin() {
    if( navigator.userAgent.match('CriOS') )
      return window.location = 'https://m.facebook.com/dialog/oauth?client_id=' + config.appId + '&redirect_uri=' +
      CURRENT_URL +'&scope=' + config.scopes.join() + '&auth_type=rerequest&response_type=token';

    FB.login(function(response) {
      if (response.status == 'connected' &&
          response.authResponse &&
          response.authResponse.grantedScopes &&
          handleMissingPermissions( response.authResponse.grantedScopes.split(',') )
      ) {
        token = response.authResponse.accessToken;
        sendLoginRequest();
      }
    }, {
      scope: config.scopes.join(),
      auth_type: 'rerequest',
      return_scopes: true
    });
  }

  function login(callback) {
    loginCallback = callback;

    if ( loginStatus === 'connected' ) {
      sendLoginRequest();
    } else {
      promptLogin();
    }
  }

  function fetchUserInfo(callback) {
    FB.api('/me', function(response) {
      callback(response);
    });
  }

  function share(url, callback) {
    FB.ui({
      method: 'share',
      href: url
    }, callback);
  }

  function feed(params, callback) {
    $.extend(params, { method: 'feed' });
    FB.ui(params, callback);
  }

  function sendAppRequest(message, callback) {
    FB.ui({
      method: 'apprequests',
      message: message
    }, callback);
  }

  function addPageTab() {
    FB.ui({
      method: 'pagetab',
      redirect_uri: CURRENT_URL
    });
  }

  function resizeCanvas(params) {
    // This method is only enabled when Canvas Height is set to "Fluid" in the App Dashboard
    FB.Canvas.setSize(params);
  }

  function disableTargetButton() {
    oldValue = $(config.targetButton).html();
    $(config.targetButton).html('Loading...').prop('disabled', true).addClass('loading');
  }

  function enableTargetButton() {
    $(config.targetButton).html( oldValue ).prop('disabled', false).removeClass('loading');
  }

  return {
    init: init,
    login: login,
    fetchUserInfo: fetchUserInfo,
    share: share,
    feed: feed,
    sendAppRequest: sendAppRequest,
    resizeCanvas: resizeCanvas
  }
})(jQuery);
(function($){  "use strict";  $('.loader').addClass('hide');  function showLoader() {    $('.loader').addClass('hide').removeClass('show');    $('body').css('overflow', 'hidden');  }  function hideLoader() {    $('.loader').addClass('hide').removeClass('show');    $('body').css('overflow', 'visible');  }  var submitProcessing = false;  $.ajaxSetup({    headers: {      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    },    beforeSend: function() {      if (submitProcessing) return false;      submitProcessing = true;    },    complete: function() {      submitProcessing = false;    },    error: function(jqXHR) {      return sweetAlert('Oups!', jqXHR.responseText, 'error');    }  });  var level, time, score, quiz, answers;  function showPage (selector) {    $(selector).addClass('show').siblings().removeClass('show');  }  facebookUtils.init({    appId: FB_APP_ID,    scopes: ['email'],    targetButton: $('.login-btn')  });  $('.login-btn').click(function() {    facebookUtils.login(function(response) {      level = response.level;      if(response.success && response.alreadyExists) {        return showPage('.level-page');      }      $('#signup-modal').modal('show');      facebookUtils.fetchUserInfo(function(response){        $('#name').val(response.name);        $('#email').val(response.email);        $('.avatar img').attr('src','http://graph.facebook.com/'+response.id+'/picture?width=200&height=200');      });    });  });  $('.level-btn').click(function(){    loadLevel( $(this).data('level') );  });  $('.validate-question').click(function() {    var num = $(this).parent('.question').data('num');    data = quiz[num];    for (var i=0; i < data.length; i++) {      if ( data[i] ==  answers[i]) {        console.log('correct');        $('#h' + i).addClass('correct');      } else if ( data.indexOf(answers[i]) ) {        console.log('wrong-position');        $('#h' + i).addClass('wrong-position');      } else {        $('#h' + i).addClass('wrong');      }    }  });  function loadLevel(level) {    $.ajax({      url: BASE_URL + "/load-level",      type: 'POST',      data: {level: level}    }).done(function(response) {      showPage('.game-page');      startGame();      game = reponse.game;      if (player['gameVideo'].paused()) {        player['gameVideo'].play();        $('#question').find('.texts li:first').text( questions[0]['question'] );        $('#question-container .answer').removeClass('show-effect');        $('.answer:eq( 0 )').text(questions[0]['answer1']);        $('.answer:eq( 1 )').text(questions[0]['answer2']);        $('#question').textillate('start');        $('#question-container').fadeIn('100');        startGame();        $('#timer, #faults').addClass('action');      }    });  }  function startGame() {    score = 0;    time = 0;    setInterval(function(){      time ++;      console.log(time);    }, 1000);  }  function showGame() {    $('#timer, #faults').removeClass('action');    seconds.animate(1);    loadQuestions();    playVideo('gameVideo');    // Initialisation    clearInterval(timer);    $('#duration').text('40');    i = 0;    tentative = 0;    $('#question-container').hide();    $('#question-container').fadeOut('50');    $('#question').find('.texts li:first').text('');    $('#faults i').removeClass('active');    $('.game').addClass('active').fadeIn().siblings().fadeOut().removeClass('active');    //var countdown = setTimeout(function () {    player['gameVideo'].on('timeupdate', function() {      if (player['gameVideo'].currentTime() > 4) {        console.log('gameshow');        player['gameVideo'].off('timeupdate');        if( window.questions !== undefined ) {          $('#question').find('.texts li:first').text(questions[0]['question']);          $('#question-container .answer').removeClass('show-effect');          $('.answer:eq( 0 )').text(questions[0]['answer1']);          $('.answer:eq( 1 )').text(questions[0]['answer2']);          $('#question').textillate('start');          $('#question-container').fadeIn('100');          startGame();          $('#timer, #faults').addClass('action');        } else {          player['gameVideo'].pause();        }      }    });    //}, 5500);  }})(jQuery);
//# sourceMappingURL=main.js.map