<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('facebook_id')->unsigned()->unique();
            $table->string('name', 100);
            $table->string('gender', 30)->nullable();
            $table->string('email')->unique();
            $table->string('phone', 8);
            $table->string('cin', 8);
            $table->integer('score');
            $table->boolean('has_shared');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('participants');
    }
}
